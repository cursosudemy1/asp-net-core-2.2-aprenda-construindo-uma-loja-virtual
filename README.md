# ASP NET Core 2.2 Aprenda construindo uma Loja Virtual

Curso ASP.NET Core 2.2 - Aprenda Construindo uma Loja Virtual




Quais recursos e tecnologias você irá aprender:

· HTTP/HTTPS e todo o ecossistema de funcionamento da internet para que os sites funcionem.

· ASP.NET Core MVC:
·· Cookie
·· Sessões
·· Filtros
·· Middleware
·· Validações customizadas
·· Área
·· Segurança (CSRF, Token, Criptografia e muito mais).

· EF Core:
·· CRUD - Criar, Consultar, Atualizar e Deletar (4 operações básicas).
·· Migrations - Criação automática de um script de criação/mudanças na estrutura do banco de dados.

· Motor de Template do ASP.NET Core - Razor:
·· Tag Helper;
·· HTML Helper;
·· ViewComponent;
·· Layout e Importações;

· Padrão:
·· MVC
·· Repository
·· Unity of Works
·· Injeção de Dependência.

· Integrações com outros sistemas/biblotecas:
·· Correios
·· Pagar.Me
·· Gmail.
·· Sistema de registro de Log (Serilog)
·· Scheduler (Agendador de tarefas - Coravel)
·· AutoMapper.

· Dicas de escalabilidade do seu sistema:

·· Escalabilidade Horizontal
·· Escalabilidade Vertical
·· Os impactos na forma de codificar.

O que você aprenderá :
A desenvolver usando ASP. NET Core 2.2 e suas APIs
A usar o Entity Framework Core e o Migrations
A trabalhar com banco de dados
As tecnologias base para desenvolver uma loja virtual
Envio de e-mail com SMTP do Gmail
Integração com o PagarMe, meio de pagamento