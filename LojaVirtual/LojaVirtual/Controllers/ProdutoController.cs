﻿using LojaVirtual.Models;
using Microsoft.AspNetCore.Mvc;

namespace LojaVirtual.Controllers
{    
    // Todo Controlador herda de Controller
    public class ProdutoController : Controller
    {
        // Todo Método Retorna um ActionResult
        public ActionResult Visualizar()
        {
            Produto produto = GetProduto();

            
            return View(produto);
        }

        private Produto GetProduto()
        {
            return new Produto()
            {
                Id = 1,
                Nome = "Xbox One X",
                Descrição = "Video Game",
                Valor = 2000.00M
            };
        }

    }
}
