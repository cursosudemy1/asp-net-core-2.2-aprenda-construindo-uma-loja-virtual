﻿using System.Data;
using System.Net;
using System.Net.Mail;
using LojaVirtual.Models;

namespace LojaVirtual.Controllers.Libraries.Email
{
    public class ContatoEmail
    {
        public static void EnviarContatoPorEmail(Contato contato)
        {
            /*
             *
             */
            SmtpClient smtp = new SmtpClient( "smtp.gmail.com",587 );
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("", ""); // informar email e senha para envio de emails
            smtp.EnableSsl = true;

            string corpoMsg = string.Format("<h2>Contato - Loja Virtual</h2>" +
                                            "<b>Nome   : {0} <br/>" +
                                            "<b>E-mail : {1} <br/>" +
                                            "<b>Texto  : {2} <br/>" +
                                            "<br/> E-mail enviado automaticamente do site LojaVirtual.", 
                                            contato.Nome, 
                                            contato.Email, 
                                            contato.Texto);
            /*
             *
             */
             MailMessage mensagem = new MailMessage();
             mensagem.From = new  MailAddress("brunoemf@gmail.com");
             mensagem.To.Add("brunoemf@gmail.com");
             mensagem.Subject = "Contato Loja Virtual";
             mensagem.Body = corpoMsg;
             mensagem.IsBodyHtml = true;

             smtp.Send(mensagem);
        }
    }
}
